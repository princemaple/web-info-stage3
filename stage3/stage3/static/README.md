H5Gaming Developer Guide
========================

- Any images should be placed in the media folder

- When you upload your images, zip the images only,
i.e. don't zip the folder, zip the images inside it.

- When you update your games' code on the website,
copy your code between the "copy start"s and "copy end"s
to the according fields (HTML, CSS, JS)

- When you reference the images, please use the path
in the following format:

	"media/abc.jpg" or "media/xyz.png"

	**e.g.**

		var img = new Image();
		img.src = "media/abc.jpg";

	**or**

		<img src="media/xyz.png" alt="this is an awesome picture!">


## Thank you for your co-operation!