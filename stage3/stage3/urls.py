from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('stage3.hosting.views',
    url(r'^$', 'home', name='home'),
    url(r'^login/?$', 'doLogin', name='login'),
    url(r'^register/?$', 'register', name='register'),
    url(r'^about/?$', 'about', name='about'),
    url(r'^user/?$', 'user', name='user'),
    url(r'^newgame/?$', 'newGame', name='newGame'),
    url(r'^games/?$', 'games', name='games'),
    url(r'^game/g_(?P<gameID>\d{8})$', 'game', name='game'),
    url(r'^game/update/g_(?P<gameID>\d{8})$', 'update', name='updateGame'),
    url(r'^game/upload/g_(?P<gameID>\d{8})$', 'upload', name='uploadAssets'),
    url(r'^newcomment/g_(?P<gameID>\d{8})$', 'newComment', name='newComment'),
    url(r'^vote/(?P<commentID>\d+)/(?P<which>\w)$', 'vote', name='vote'),
    url(r'^issue/g_(?P<gameID>\d{8})$', 'issue', name='issue'),
    url(r'^issue/(?P<issueID>\d+)/resolve$', 'resolve', name='resolveIssue'),
    url(r'^newissue/g_(?P<gameID>\d{8})$', 'newIssue', name='newIssue'),
    url(r'^issue/g_(?P<gameID>\d{8})/(?P<issueID>\d+)/(?P<counter>\d+)$',
        'issueDetail', name='issueDetail'),
    url(r'^issue/newcomment/(?P<issueID>\d+)$', 'newIssueComment',
        name='newIssueComment'),

    url(r'^admin/?', include(admin.site.urls)),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
) + patterns('',
    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
)

if settings.DEBUG:
	urlpatterns += patterns('django.views.static',
        (r'^static/(?P<path>.*)$', 'serve'),
	    (r'^media/(?P<path>.*)$', 'serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
	) + patterns('stage3.hosting.views',
        url(r'^.*$', 'handler404', name='handler404'),
    )
