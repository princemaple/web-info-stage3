from django.db import models
from django.contrib.auth.models import AbstractUser

class InfoUser(AbstractUser):
	reputation = models.IntegerField(blank=True, null=True)

	def __unicode__(self):
		return self.username

class Category(models.Model):
	name = models.CharField(max_length=40)
	description = models.CharField(max_length=200)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ["name"]

class Game(models.Model):
	title = models.CharField(db_index=True, max_length=100, unique=True)
	html = models.TextField(blank=True)
	css = models.TextField(blank=True)
	js = models.TextField(blank=True)
	creation = models.DateTimeField(db_index=True)
	update = models.DateTimeField()
	inspection = models.IntegerField()
	category = models.ForeignKey(Category)
	description = models.CharField(max_length=200, blank=True, null=True)
	developer = models.ForeignKey(InfoUser)

	class Meta:
		ordering = ['-creation']

	def __unicode__(self):
		return self.title

class Comment(models.Model):
	content = models.CharField(max_length=1200)
	creation = models.DateTimeField()

	class Meta:
		abstract = True

class Issue(models.Model):
	title = models.CharField(max_length=120)
	content = models.CharField(max_length=1500)
	resolved = models.BooleanField()
	user = models.ForeignKey(InfoUser)
	game = models.ForeignKey(Game)

	class Meta:
		ordering = ['-id']

	def __unicode__(self):
		return "issue %d - " %self.id + self.title

class GameComment(Comment):
	user = models.ForeignKey(InfoUser, blank=True, null=True)
	rating = models.SmallIntegerField(blank=True, null=True)
	game = models.ForeignKey(Game)
	replyto = models.ForeignKey('self', null=True, blank=True)

	class Meta:
		ordering = ['-creation']

class IssueComment(Comment):
	user = models.ForeignKey(InfoUser)
	issue = models.ForeignKey(Issue)

class Vote(models.Model):
	val = models.SmallIntegerField()
	comment = models.ForeignKey(GameComment)
	user = models.ForeignKey(InfoUser)
