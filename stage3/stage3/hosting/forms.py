from django import forms
from django.forms import Form, ModelForm
from django.conf import settings

from stage3.hosting.models import Game, InfoUser, Issue
from captcha.fields import ReCaptchaField

import re

class NewGameForm(ModelForm):
	class Meta:
		model = Game
		fields = ('title', 'category', 'description')
		widgets = {
			'description': forms.Textarea(attrs={'rows':3})
		}

class IssueForm(ModelForm):
	class Meta:
		model = Issue
		fields = ('title', 'content')
		widgets = {
			'content': forms.Textarea(attrs={'rows': 10, 'cols': 50})
		}

class EditGameForm(ModelForm):
	class Meta:
		model = Game
		fields = ('description', 'html', 'css', 'js')
		widgets = {
			'description': forms.Textarea(attrs={'rows': 5}),
			'html': forms.Textarea(attrs={'rows': 5}),
			'css': forms.Textarea(attrs={'rows': 5}),
			'js': forms.Textarea(attrs={'rows': 5})
		}

	def clean_html(self):
		html = self.cleaned_data['html']
		script = re.search(r'<script', html)
		style = re.search(r'<style', html)
		if script:
			raise forms.ValidationError("Script tag not needed and not allowed. Your javascript will be included automatically.")
		if style:
			raise forms.ValidationError("Inline CSS is not allowed, please leave them in the separate CSS file(field).")
		return html

	def clean_js(self):
		js = self.cleaned_data['js']
		url = re.findall(r"\"/?media/g_(\d{8})", js)
		gid = self.instance.id
		if any([int(x) != gid for x in url]):
			raise forms.ValidationError(
				"You are not allowed to have links to assets that belong to other games! The ID for this game is `%08d`, not including the \"`\"s."% gid)
		js = re.sub(r"\"/?(media/(?!g_))", r'"/\1g_%08d/' %self.instance.id, js)
		js = re.sub(r"\"/?(\w*(?:\.jpg|\.png))", r'"/media/g_%08d/\1' %self.instance.id, js)
		eval = re.search(r"\beval\b", js)
		if eval:
			raise forms.ValidationError("No `eval` function is allowed! Please avoid using this name in your game. Thank you.")

		return js

class UploadFileForm(Form):
	asset_file = forms.FileField(help_text='NOTE: Please upload a zip file that contains all the image assets, maximum size of 2MB. All images must be either jpg or png. Only these two types of images will be extracted to the file system. Icon must be a png file and named as "icon.png" if exists, 140x140 for best effect.')

	def clean_asset_file(self):
		f = self.cleaned_data['asset_file']
		if not f.name.endswith(".zip"):
			raise forms.ValidationError(
				"File must be a zipfile, with .zip suffix")
		if f.size > settings.FILE_SIZE:
			raise forms.ValidationError("File must be at most 2MB in size")
		return f

class RegistrationForm(ModelForm):
	class Meta:
		model = InfoUser
		fields = ('username', 'email', 'password')
		widgets = {
			'password': forms.PasswordInput
		}
	password_repeat = forms.CharField(
		widget=forms.PasswordInput,
		max_length=128,
		help_text='This has to be the same as the password above')
	captcha = ReCaptchaField()

	def clean_password_repeat(self):
		pw1 = self.cleaned_data['password']
		pw2 = self.cleaned_data['password_repeat']

		if len(pw1) < 8:
			raise forms.ValidationError("Please pick a better password that is at least 8 characters long")

		if pw1 != pw2:
			raise forms.ValidationError("Your passwords don't match")

		return pw2
