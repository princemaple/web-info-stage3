from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from stage3.hosting.models import\
	InfoUser, Game, Category, Issue, GameComment, IssueComment, Vote

class InfoUserAdmin(UserAdmin):
	def __init__(self, *args, **kwargs):
		super(InfoUserAdmin, self).__init__(*args, **kwargs)
		self.fieldsets[1][-1]['fields'] = (
			'first_name', 'last_name', 'email', 'reputation')

admin.site.register(InfoUser, InfoUserAdmin)
admin.site.register(Game)
admin.site.register(Category)
admin.site.register(Issue)
admin.site.register(GameComment)
admin.site.register(IssueComment)
admin.site.register(Vote)
