from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required#, user_passes_test
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings
from django.db.models import Sum
from django.utils.timezone import now

from stage3.hosting.forms import \
	IssueForm, NewGameForm, EditGameForm, RegistrationForm, UploadFileForm
from stage3.hosting.models import \
	Game, Category, GameComment, Vote, Issue, IssueComment

from zipfile import ZipFile
from uuid import uuid4
import os, shutil, json

# media path
MEDIA = settings.MEDIA_ROOT
# static path
STATIC = os.path.join(settings.SETTING_DIR, "static")
# games per page - float for computation purpose
GPP = 8.0
# Voting translation
voteDict = {'u': (1, "Up Vote"), 'd': (-1, "Down Vote")}

def home(request):
	return render(request, "home.html")

from math import ceil
def games(request):
	offset = request.GET.get('offset', 0)
	offset = int(offset)
	q = request.GET.get('q', None)
	if q:
		games = Game.objects.filter(title__icontains=q)
		l = games.count()
		games = games[offset:offset+GPP]
		url = "/games?offset=%d&q=" + q
	else:
		category = request.GET.get('category', None)
		if category:
			category = Category.objects.get(name__iexact=category)
			games = Game.objects.filter(category=category)
			l = games.count()
			games = games[offset:offset+GPP]
		else:
			games = Game.objects.all()
			l = games.count()
			games = games[offset:offset+GPP]
		url = "/games?offset=%d" + \
			(category and "&category=" + category.name or "")
	pages = int(ceil(l / GPP))
	pages = range(pages)
	pages = [(p+1, url %(p * GPP)) for p in pages]
	current = offset / GPP + 1
	categories = Category.objects.all()
	d = {'games': games, 'categories': categories,
		'pages': pages, 'current': current}
	return render(request, "games.html", d)

def doLogin(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user is not None and user.is_active:
			next = request.GET.get('next', '/')
			login(request, user)
			return redirect(next)
		else:
			form = AuthenticationForm(request.POST)
			errors = ["Something is wrong", "Please try again"]
			d = {'form': form, 'errors': errors}
			return render(request, "login.html", d)
	form = AuthenticationForm()
	return render(request, "login.html", {'form': form})

def register(request):
	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			logout(request)
			member = form.save(commit=False)
			un = form.cleaned_data['username']
			pw = form.cleaned_data['password']
			member.set_password(pw)
			member.reputation = 0
			member.save()
			user = authenticate(username=un, password=pw)
			login(request, user)
			return redirect("/")
		else:
			return render(request, "form.html",
				{"title": "Register", "form": form})
	form = RegistrationForm()
	return render(request, "form.html",
		{"title": "Register", "form": form})

@login_required(login_url='/login')
def user(request):
	offset = request.GET.get('offset', 0)
	offset = int(offset)
	user = request.user
	games = Game.objects.filter(developer=user.id)
	l = games.count()
	games = games[offset:offset + GPP]
	pages = int(ceil(l / GPP))
	pages = range(pages)
	url = "user?offset=%d"
	pages = [(p+1, url %(p * GPP)) for p in pages]
	current = offset / GPP + 1
	d = {'games': games, 'pages': pages, 'current': current}
	return render(request, "user.html", d)

def permCheck(request, user, game):
	if game.developer != user:
		error = "This is not your game."
		title = "Error - Permission"
		d = {'error': error, 'title': title}
		return (False, render(request, "error.html", d))
	return (True, None)

@login_required(login_url='/login')
def update(request, gameID):
	game = Game.objects.get(id=int(gameID))
	user = request.user
	access, res = permCheck(request, user, game)
	if not access: return res

	title = 'Update Your Game'
	if request.method == 'POST':
		form = EditGameForm(request.POST, instance=game)
		if form.is_valid():
			game = form.save(commit=False)
			game.update = now()
			game.save()
			gamePath = os.path.join(MEDIA, 'g_' + gameID)
			protected = os.path.join(gamePath, 'protected')
			cssPath = os.path.join(protected, gameID + '.css')
			jsPath = os.path.join(protected, gameID + '.js')
			with open(cssPath, "wb+") as f:
				f.write(game.css.encode('utf-8'))
			with open(jsPath, "wb+") as f:
				f.write(game.js.encode('utf-8'))
		else:
			d = {'form': form, 'title': title}
			return render(request, "form.html", d)
		return redirect("/user")

	form = EditGameForm(instance=game)
	d = {'form': form, 'title': title}
	return render(request, "form.html", d)

@login_required(login_url='/login')
def upload(request, gameID, status=None):
	user = request.user
	game = Game.objects.get(id=int(gameID))
	access, res = permCheck(request, user, game)
	if not access: return res

	title = 'Upload Assets'
	if request.method == 'POST':
		form = UploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			gamePath = os.path.join(MEDIA, "g_" + gameID)
			for fn in os.listdir(gamePath):
				tp = os.path.join(gamePath, fn)
				if os.path.isfile(tp):
					os.remove(tp)
			des = os.path.join(gamePath, uuid4().get_hex())
			f = form.cleaned_data['asset_file']
			with open(des, "wb+") as uploadfile:
				uploadfile.write(f.read())
			z = ZipFile(des)
			for zf in z.namelist():
				if zf.endswith(".png") or zf.endswith(".jpg"):
					z.extract(zf, path=gamePath)
			z.close()
			os.remove(des)
			if os.path.exists(os.path.join(gamePath, 'icon.png')):
				newIcon = os.path.join(gamePath, 'icon.png')
				protected = os.path.join(gamePath, 'protected')
				protectedIcon = os.path.join(protected, 'icon.png')
				os.remove(protectedIcon)
				os.rename(newIcon, protectedIcon)
			return redirect('/user')
		else:
			d = {'form': form, 'title': title}
			return render(request, "form.html", d)
	form = UploadFileForm()
	d = {'form': form, 'title': title}
	return render(request, 'form.html', d)

@login_required(login_url='/login')
def newGame(request):
	if request.method == 'POST':
		form = NewGameForm(request.POST)
		if form.is_valid():
			game = form.save(commit=False)
			game.creation = now()
			game.update = now()
			game.developer = request.user
			game.inspection = request.user.is_staff and 99 or 0
			game.save()
			gameID = "%08d" %game.id
			p = os.path.join(MEDIA, "g_" + gameID)
			pp = os.path.join(p, "protected")
			os.mkdir(p)
			os.mkdir(pp)
			defaultIcon = os.path.join(STATIC, 'icon.png')
			targetPath = os.path.join(pp, 'icon.png')
			css = os.path.join(pp, gameID + '.css')
			js = os.path.join(pp, gameID + '.js')
			with open(css, "w") as f:
				f.write("\n")
			with open(js, "w") as f:
				f.write("\n")
			shutil.copyfile(defaultIcon, targetPath)
			return redirect('/user')
		else:
			d = {'form': NewGameForm(request.POST),
				'title': 'Create New Game'}
			return render(request, 'form.html', d)
	form = NewGameForm()
	d = {'form': form, 'title': 'Create New Game'}
	return render(request, 'form.html', d)

def game(request, gameID):
	gid = int(gameID)
	game = Game.objects.get(id=gid)
	category = game.category.name
	topComments = GameComment.objects.filter(game=game) \
				.annotate(votes=Sum('vote__val')) \
				.order_by('-votes', '-creation')
	for c in topComments:
		if c.votes == None:
			c.votes = 0
	topComments = sorted(topComments, reverse=True, key=lambda x: x.votes)
	d = {'content': game.html,
		'gameID': gameID,
		'game': game,
		'comments': topComments,
		'category': category}
	return render(request, "gameFrame.html", d)

def newComment(request, gameID):
	d = {}
	if request.is_ajax() and request.method == 'POST':
		data = request.POST
		rating = data.get('rating', None)
		content = data.get('content', ' ')
		replyTo = data.get('replyTo', None)
		auth = request.user.is_authenticated()
		if rating:
			if not auth:
				return redirect('/login?next=/game/g_%s' %gameID)
			rating = int(rating)
		game = Game.objects.get(id=int(gameID))
		comment = GameComment(game=game, user=auth and request.user or None)
		comment.rating = rating
		comment.content = content
		comment.creation = now()
		comment.replyto = replyTo
		comment.save()
		d = {'rating': rating, 'conent': content, 'replyTo': replyTo}
	return HttpResponse(json.dumps(d), mimetype="application/json")

@login_required(login_url='/login')
def vote(request, commentID, which):
	comment = GameComment.objects.get(id=int(commentID))
	vote = Vote.objects.filter(comment=comment, user=request.user)
	val, message = voteDict.get(which, (0, "Error"))
	if vote.exists():
		vote = vote[0]
		if vote.val == val:
			vote.val = 0
			vote.save()
			message += " is successfully canceled."
		else:
			vote.val = val
			vote.save()
			message = "Vote is successfully changed to " + message
	else:
		if request.user == comment.user:
			message += " is not done, because you are voting for yourself."
		else:
			vote = Vote(val=val, comment=comment, user=request.user)
			vote.save()
			message += " is successfully done."
	d = json.dumps({'message': message})
	return HttpResponse(d, mimetype='application/json')

def issue(request, gameID):
	game = Game.objects.get(id=(int(gameID)))
	issues = Issue.objects.filter(game=game)

	d = {'content': issues,
		'gameID': gameID}

	return render(request, "issue.html", d)

@login_required(login_url='/login')
def newIssue(request, gameID):
	if request.method == 'POST':
		game = Game.objects.get(id=int(gameID))
		if game is None:
			return redirect('/404')
		form = IssueForm(request.POST)
		if form.is_valid():
			issue = form.save(commit=False)
			issue.user = request.user
			issue.game = game
			issue.resolved = False
			issue.save()
			return redirect('/issue/g_%s' %gameID)
		else:
			form = IssueForm(request.POST)
			return render(request, "form.html", {'form': form})
	form = IssueForm()
	d = {'form': form}
	return render(request, "form.html", d)

def resolve(request, issueID):
	issue = Issue.objects.get(id=int(issueID))
	issue.resolved = not issue.resolved
	issue.save()
	return HttpResponse("OK")

def issueDetail(request, gameID, issueID, counter):
	game = Game.objects.get(id=int(gameID))
	issue = Issue.objects.get(id=int(issueID))
	comments = IssueComment.objects.filter(issue=issue)
	d = {'game': game,
		'issue': issue,
		'comments': comments,
		'counter': counter}
	return render(request, "issueDetail.html", d)

# @login_required(login_url='/login')
def newIssueComment(request, issueID):
	if request.user.is_authenticated():
		content = request.POST.get('content', '')
		if 0 < len(content) <= 1200:
			issue = Issue.objects.get(id=int(issueID))
			ic = IssueComment(
				user=request.user,
				content=content,
				creation=now(),
				issue=issue)
			ic.save()
			success, message = True, ""
		else:
			success, message = (
				False, "Your comment cannot be empty or more than 1200 characters")
	else:
		success, message = False, "You have to login before you can comment, you may want to save your comment before you go to login page."
	d = {'message': message, 'success': success}
	return HttpResponse(json.dumps(d), mimetype='application/json')

def about(request):
	return render(request, "about.html")

def handler404(request):
	error = "Page Not Found"
	title = "Error 404"
	d = {'error': error, 'title': title}
	return render(request, "error.html", d)
